import 'package:petani/minggu6/mainToBasicStyling.dart';
import 'package:petani/minggu6/route.dart';
import 'package:flutter/material.dart';
import 'package:petani/minggu7/gradient.dart';
import 'package:petani/minggu_7(pasca_uts)/form.dart';
import 'package:petani/minggu_7(pasca_uts)/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'controller/demoController.dart';
import 'view/demoPage.dart';
import 'view/home.dart';

void main() async {
  // runApp(MaterialApp(
  //   onGenerateRoute: RouteGenerator.generateRoute,
  // ));
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final DemoController ctrl = Get.put(DemoController());
  @override
  Widget build(BuildContext context) {
    return SimpleBuilder(builder: (_) {
      return GetMaterialApp(
        title: 'GetX',
        theme: ctrl.theme,
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => HomePage(),
          '/cart': (context) => DemoPage(),
        },
      );
    });
  }
}
