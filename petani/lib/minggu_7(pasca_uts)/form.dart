import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BelajarForm extends StatefulWidget {
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitc = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Belajar Form Flutter"),
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      autofocus: true,
                      decoration: new InputDecoration(
                        hintText: "masukan nama lengkap anda",
                        labelText: "Nama Lengkap",
                        icon: Icon(Icons.people),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Nama tidak boleh kosong";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: new InputDecoration(
                        hintText: "contoh: 0812xxxxxxx",
                        labelText: "Nomor Telp",
                        icon: Icon(Icons.phone),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return " Telepon tidak boleh kosong";
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      obscureText: true,
                      keyboardType: TextInputType.phone,
                      decoration: new InputDecoration(
                        hintText: "isi password",
                        labelText: "Password",
                        icon: Icon(Icons.password),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return " Password tidak boleh kosong";
                        }
                        return null;
                      },
                    ),
                  ),
                  CheckboxListTile(
                    title: Text('Belajar Flutter'),
                    subtitle: Text('Dart,Widget, Htpp'),
                    value: nilaiCheckBox,
                    activeColor: Colors.blueAccent,
                    onChanged: (value) {
                      setState(() {
                        nilaiCheckBox = value!;
                      });
                    },
                  ),
                  SwitchListTile(
                    title: Text('Backend Programming'),
                    subtitle: Text('Dart, NodeJS,PHP, Java,dll'),
                    value: nilaiSwitc,
                    activeTrackColor: Colors.lightBlueAccent,
                    activeColor: Colors.blueAccent,
                    onChanged: (value) {
                      setState(() {
                        nilaiSwitc = value;
                      });
                    },
                  ),
                  Slider(
                      value: nilaiSlider,
                      min: 0,
                      max: 100,
                      onChanged: (value) {
                        setState(() {
                          nilaiSlider = value;
                        });
                      }),
                  RaisedButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.blue,
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {}
                    },
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
