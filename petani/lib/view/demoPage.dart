import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:petani/controller/demoController.dart';

class DemoPage extends StatelessWidget {
  final DemoController ctrl = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Demo Page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
                padding: const EdgeInsets.all(8.0), child: Text(Get.arguments)),
            SwitchListTile(
              value: ctrl.isDark,
              title: Text("Switch to change ThemeMode"),
              onChanged: ctrl.changeTheme,
            ),
            Padding(padding: const EdgeInsets.all(8.0)),
            ElevatedButton(
                onPressed: () => Get.snackbar(
                    "Snackbar", "Hello this is the Snackbar message",
                    snackPosition: SnackPosition.BOTTOM,
                    colorText: Colors.white,
                    backgroundColor: Colors.black87),
                child: Text('Snack Bar')),
            Padding(padding: const EdgeInsets.all(8.0)),
            ElevatedButton(
                onPressed: () => Get.defaultDialog(
                    title: "Dialogue",
                    content: Text(
                      "Hey, From Dialogue",
                    )),
                child: Text('Dialogue')),
            Padding(padding: const EdgeInsets.all(8.0)),
            ElevatedButton(
                onPressed: () => Get.bottomSheet(Container(
                      height: 150,
                      color: Colors.blueAccent,
                      child: Text(
                        'Hello From Bottom Sheet',
                        style: TextStyle(fontSize: 30.0),
                      ),
                    )),
                child: Text('Bottom Sheet')),
            Padding(padding: const EdgeInsets.all(8.0)),
            ElevatedButton(
                onPressed: () => Get.offNamed('/'),
                child: Text('Back To Home')),
          ],
        ),
      ),
    );
  }
}
