import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:petani/model/product.dart';

class Purchase extends GetxController {
  var products = <Product>[].obs;
  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    await Future.delayed(Duration(seconds: 1));
    var serverResponse = [
      Product(
          1,
          "First Product",
          "aby",
          "This is a Product that we are going to show by getX",
          "300.0",
          300.0),
      Product(
          1,
          "Second Product",
          "aby",
          "This is a Product that we are going to show by getX",
          "350.0",
          350.0),
      Product(
          1,
          "Third Product",
          "aby",
          "This is a Product that we are going to show by getX",
          "400.0",
          400.0),
      Product(
          1,
          "Fourth Product",
          "aby",
          "This is a Product that we are going to show by getX",
          "450.0",
          450.0),
      Product(
          1,
          "Fifth Product",
          "aby",
          "This is a Product that we are going to show by getX",
          "500.0",
          500.0),
      Product(
          1,
          "Demo Product",
          "aby",
          "This is a Product that we are going to show by getX",
          "550.0",
          550.0),
      Product(
          1,
          "New Product",
          "aby",
          "This is a Product that we are going to show by getX",
          "600.0",
          600.0),
    ];
    products.value = serverResponse;
  }
}
